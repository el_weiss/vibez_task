import { ProjectContext } from "contexts/ProjectContext";
import { useContext } from "react";

export const useProjectContext = () => {
    const context = useContext(ProjectContext);

    if (!context) {
        throw Error("useProjectContext must be used inside ProjectContextProvider");
    }

    return context;
};
