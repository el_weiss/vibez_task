import {
    Button,
    Flex,
    Text,
    useColorModeValue,
  } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { product_type_dict, currency_dict } from "dictionaries";

export default function ProductsRow({
    item,
    toggleClosure,
    editedItem
}) {

    const [product, setProduct] = useState(item);
    const textColor = useColorModeValue("secondaryGray.900", "white");

    useEffect(() => {
      if(item.id === editedItem.id) {
        setProduct(editedItem);
      }
    }, [editedItem])

    return (
        <tr key={product.id}>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product.name}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product.status}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product_type_dict[product?.product_type]}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product.description}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {currency_dict["ILS"]}
                  {product.price}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product?.stock}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product?.reserved}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product?.sold}
                </Text>
              </Flex>
            </td>

            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Button
                  onClick={() => toggleClosure(product)}
                  variant={"brand"}
                  minW={"100px"}
                  size="md"
                  fontWeight="500"
                  borderRadius="70px"
                  mx="10px"
                >
                  Edit
                </Button>
              </Flex>
            </td>
        </tr>
    )
}