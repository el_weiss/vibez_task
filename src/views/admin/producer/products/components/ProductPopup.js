// Chakra imports
import {
  Avatar,
  Button,
  Flex,
  Icon,
  Image,
  Link,
  Text,
} from "@chakra-ui/react";
// Custom components
import Card from "components/card/Card.js";
// Custom icons
import React, { useState, useEffect } from "react";
import { MdDateRange, MdDownload, MdCancel } from "react-icons/md";
import { HSeparator } from "components/separator/Separator";
import { normalizeDate } from "utils/datetime";
import { Social } from "components/social/social";
import { calculateAge } from "utils/datetime";
import FormCard from "./FormCard";

export function ProductPopup(props) {
  const {user, path, product, fetchProducts, toggleClosure, setEditedItem } = props;

  return (
    <Flex
      w="100%"
      h="100%"
      position={"fixed"}
      right="0px"
      top="0px"
      zIndex={999999}
    >
      <Flex flex={1}></Flex>
      <Flex
        width="30px"
        bg="linear-gradient(to right, white , lightgrey)"
      ></Flex>
      <Flex
        width="40%"
        minW="375px"
        bgColor="white"
        borderLeftWidth={1}
        borderLeftColor="2A2A2A"
        direction={"column"}
      >
        <Flex id="popup_top" m="40px">
          <Button onClick={() => toggleClosure()} w="20px" h="20px">
            X
          </Button>
        </Flex>
        <Flex w="100%" h="100%" flex={1} justifyContent={"space-around"}>
          <FormCard
            path={path}
            user={user}
            product={product}
            fetchProducts={fetchProducts}
            toggleClosure={toggleClosure}
            setEditedItem={setEditedItem}
          />
        </Flex>
      </Flex>
    </Flex>
  );
}
