/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";

// Chakra imports
import {
  Flex,
  useColorModeValue,
  FormControl,
  Button,
  Text,
  Select,
} from "@chakra-ui/react";
import {
  renderThumb,
  renderTrack,
  renderView,
} from "components/scrollbar/Scrollbar";
import { Scrollbars } from "react-custom-scrollbars-2";
import Card from "components/card/Card.js";
import InputField from "components/fields/InputField";
import TextField from "components/fields/TextField";
import { DeleteButton } from "components/buttons/DeleteButton";

// Custom components
import { product_type_dict } from "dictionaries";
import Label from "../../../../../components/fields/Label";

export default function FormCard({
  product,
  path,
  fetchProducts,
  toggleClosure,
  user,
  setEditedItem
}) {
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const textColorSecondary = "secondaryGray.600";
  const [name, setName] = useState(product?.name);
  const [status, setStatus] = useState(product?.status ? product?.status : "");
  const [type, setType] = useState(
    product?.product_type ? product?.product_type : ""
  );
  const [description, setDescription] = useState(product?.description);
  const [price, setPrice] = useState(product ? product.price : 0);
  const [stock, setStock] = useState(product ? product.stock : 0);
  const [errorMsg, setErrorMsg] = useState("");
  const [inProgress, setInProgress] = useState(false);
  const hash = window.location.hash;

  const showError = () => {
    setErrorMsg("You can't delete the Product");
    setInterval(() => {
      setErrorMsg("");
    }, 2000);
  };

  const handleDelete = async (item) => {
    if (inProgress) return;
    if (!user) {
      setErrorMsg("You not authorized. Try login again.");
      return;
    }
    setInProgress(true);
    const response = await fetch(path + "/" + item.id, {
      method: "DELETE",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
    });
    const json = await response.json();
    console.log("DELETE ITEM RES: ", json);
    setInProgress(false);
    if (response.ok) {
      fetchProducts(path);
    } else {
      showError();
    }
  };

  const handleSave = async () => {
    setErrorMsg("");
    if (inProgress) return;
    if (!user) {
      setErrorMsg("You not authorized. Try login again.");
      return;
    }
    if (!status) {
      setErrorMsg("Please select status.");
      return;
    }
    if (!type) {
      setErrorMsg("Please select product type.");
      return;
    }
    setInProgress(true);
    const body = {
      product: {
        name: name,
        description: description,
        product_type: type,
        status: status,
        price: price,
        stock: stock,
      },
    };
    console.log("CREATE PRODUCT BODY: ", path, JSON.stringify(body));
    let hash = window.location.hash;
    const response = await fetch(product ? path + "/" + product.id : path, {
      method: product ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: user,
      },
      body: JSON.stringify(body),
    });
    const json = await response.json();
    console.log("JSON: ", JSON.stringify(json));
    setInProgress(false);
    if (!response.ok) {
      setErrorMsg(json.error.message);
    }
    if (response.ok) {
      console.log("RES: ", JSON.stringify(json));
      toggleClosure();
      if (product) {
        setEditedItem(json.product);
      } else {
        fetchProducts(path);
      }
    }
    return;
  };

  // Chakra Color Mode
  return (
    <FormControl w="60%" minW="335px">
      <Scrollbars
        autoHide
        renderTrackVertical={renderTrack}
        renderThumbVertical={renderThumb}
        renderView={renderView}
      >
        <Flex direction={"column"} mb="20px" pl="5px" pr="10px" pb="20px">
          <InputField
            required={true}
            mb={"10px"}
            // me="30px"
            id="name"
            label="Product name"
            placeholder="The best product"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <Label label={"Product type"} required={true} />
          <Select
            onChange={(e) => setType(e.target.value)}
            fontSize="sm"
            id="edit_product"
            variant="main"
            h="44px"
            w="20%"
            minW="200px"
            mb={"8px"}
            maxh="44px"
            value={type}
            me={{ base: "10px", md: "20px" }}
          >
            <option disabled={true} value="">
              Select
            </option>
            <option value="entry_ticket">
              {product_type_dict["entry_ticket"]}
            </option>
            <option value="child_ticket">
              {product_type_dict["child_ticket"]}
            </option>
            <option value="transport_ticket">
              {product_type_dict["transport_ticket"]}
            </option>
            <option value="merchandise">
              {product_type_dict["merchandise"]}
            </option>
            <option value="drinks_and_food">
              {product_type_dict["drinks_and_food"]}
            </option>
            <option value="other">{product_type_dict["other"]}</option>
          </Select>

          <TextField
            onChange={(e) => setDescription(e.target.value)}
            value={description}
            id="description"
            label="Description"
            h="100px"
            // placeholder="Tell something about yourself in 150 characters!"
          />
          <Label label={"Status"} required={true} />
          <Select
            onChange={(e) => setStatus(e.target.value)}
            fontSize="sm"
            id="edit_product"
            variant="main"
            h="44px"
            w="20%"
            minW="200px"
            mb={"8px"}
            maxh="44px"
            value={status}
            me={{ base: "10px", md: "20px" }}
          >
            <option disabled={true} value="">
              Select
            </option>
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
            <option value="hidden">Hidden</option>
          </Select>
          <InputField
            required={true}
            mb="10px"
            me="30px"
            id="name"
            label="Ticket type price"
            placeholder="The best product"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
          />
          <InputField
            mb="10px"
            me="30px"
            id="name"
            label="Max number of this ticket type"
            value={stock}
            onChange={(e) => setStock(e.target.value)}
          />
          <Text color="red" h="20px">
            {errorMsg}
          </Text>
          <Flex direction={"row"} mt="10px">
            {product && (
              <DeleteButton
                action={() => {
                  if (!inProgress) handleDelete(product);
                }}
                isPassive={true}
              />
            )}
            <Button
              onClick={toggleClosure}
              variant={"outline"}
              minW={"100px"}
              size="md"
              fontWeight="500"
              borderRadius="70px"
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                if (!inProgress) handleSave();
              }}
              variant={"brand"}
              minW={"100px"}
              size="md"
              fontWeight="500"
              borderRadius="70px"
              mx="10px"
            >
              Save
            </Button>
          </Flex>
        </Flex>
      </Scrollbars>
    </FormControl>
  );
}
