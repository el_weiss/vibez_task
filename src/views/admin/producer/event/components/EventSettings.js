/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";

// Chakra imports
import {
  Flex,
  useColorModeValue,
  FormControl,
  Button,
  Text,
} from "@chakra-ui/react";
import Card from "components/card/Card.js";
import SliderThumbWithTooltip from "./Slider";

// Custom components

export default function EventSettings({ event_path, user, setEvent, event }) {
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const textColorSecondary = "secondaryGray.600";
  const [currentEvent, setCurrentEvent] = useState(null);
  const [onTopFee, setOnTopFee] = useState(event?.on_top_fee);
  const [errorMsg, setErrorMsg] = useState("");
  const [inProgress, setInProgress] = useState(false);

  const handleSave = async () => {
    if (inProgress) return;
    if (!user) return;
    setErrorMsg("");
    setInProgress(true);
    const body = {
      event: {
        on_top_fee: onTopFee,
      },
    };
    console.log("UPDATE EVENT SETTINGS: ", JSON.stringify(body));
    const response = await fetch(event_path, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: user,
      },
      body: JSON.stringify(body),
    });
    const json = await response.json();
    console.log("JSON: ", JSON.stringify(json));

    if (!response.ok) {
      setErrorMsg(json.error.message);
    }
    if (response.ok) {
      setEvent(json.event);
      setCurrentEvent(json.event);
      console.log("RES: ", JSON.stringify(json));
    }
    setInProgress(false);
    return;
  };

  useEffect(() => {
    localStorage.setItem("event_route", "settings");
    if (event) setCurrentEvent(event);
  }, [event]);

  // Chakra Color Mode
  return (
    <FormControl w="60%" minW="300px">
      <Card mb="20px">
        <Text
          fontSize="sm"
          color={textColorPrimary}
          fontWeight="bold"
          marginInlineStart={"10px"}
          mb={"8px"}
          alignSelf={"flex-start"}
        >
          On Top Fee: {onTopFee}
        </Text>

        <Flex direction={"column"}>
          <SliderThumbWithTooltip value={onTopFee} set_value={setOnTopFee} />
          <Text color="red">{errorMsg}</Text>
          <Flex direction={"row-reverse"} ms="10px" mt="40px">
            <Button
              onClick={() => setOnTopFee(event?.on_top_fee)}
              variant={"outline"}
              minW={"100px"}
              size="md"
              fontWeight="500"
              borderRadius="70px"
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                if (!inProgress) handleSave();
              }}
              variant={"brand"}
              minW={"100px"}
              size="md"
              fontWeight="500"
              borderRadius="70px"
              mx="10px"
            >
              Save
            </Button>
          </Flex>
        </Flex>
      </Card>
    </FormControl>
  );
}
