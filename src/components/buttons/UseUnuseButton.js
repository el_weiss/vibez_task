import React, { useState } from "react";

// Chakra imports
import { Button } from "@chakra-ui/react";

// Custom components

export function UseUnuseButton({ isUsed, user, setError, path, action }) {
  const [inProgress, setInProgress] = useState(false);
  const toggleUsage = async () => {
    if (inProgress) return;
    setError(null);
    if (!user) {
      setError("You not authorized. Try login again.");
      return;
    }
    setInProgress(true);
    let body = { item: { used: !isUsed } };
    console.log("BODY: ", body, path);
    const response = await fetch(path, {
      method: "PUT",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
      body: JSON.stringify(body),
    });
    const json = await response.json();
    console.log("TOGGLE TICKET RES: ", json);
    setInProgress(false);
    if (response.ok) {
      action(json.item.used);
    } else {
      setError(response?.error?.message);
    }
  };
  // Chakra Color Mode
  return (
    <Button
      onClick={() => {
        if (!inProgress) toggleUsage();
      }}
      variant={"brand"}
      maxW="140px"
      size="md"
      fontSize="sm"
      fontWeight="500"
      borderRadius="70px"
      px="14px"
      py="5px"
    >
      {isUsed ? "UNUSE TICKET" : "USE TICKET"}
    </Button>
  );
}
